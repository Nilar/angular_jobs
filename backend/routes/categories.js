var express = require('express');
var router = express.Router();
var models = require("../models");

/* GO List page. */
router.get('/',async function(req, res, next) {
  let cats = await models.Category.findAll();
  
  res.render('category/index', { data: cats });
});

/* GO Create page. */
router.get('/create',async function(req, res, next) {
 
  res.render('category/create');
});

// Edit Form
router.get('/edit/:id',async function(req, res, next) {
  let id = req.params.id || 0;
  let cats = await models.Category.findOne({ where: {id: id} });
  res.render('category/edit',{ data: cats });
});

// Update Category
/* Store Category */
router.post('/update/:id', async function(req, res, next) {

  let formData = req.body;
  let id = req.params.id || 0;
  
let status = await models.Category.update(formData, { where : { id : id }});
res.redirect('/categories');

});

//Store Create
router.post('/create', async function(req, res, next) {

  let formData = req.body;
  
  
let status = await models.Category.create(formData);
res.redirect('/categories');

});
// Store Delete
router.post('/delete/:id', async function(req, res, next) {
  let id = req.params.id || 0;
  let status = await models.Category.destroy({ where : { id : id}});
  res.redirect('/categories');
});
module.exports = router;
