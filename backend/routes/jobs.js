var express = require('express');
var router = express.Router();
var models = require("../models");

/* GET home page. */
router.get('/', async function(req, res, next){
  let conditions ={
    include:[
        {
        model:models.Category
        },
        {
            model:models.User,
            attributes: { exclude: ['password','status'] }
        }]
    };
    let category = req.query.category;

    if(category){
        conditions.where = { category_id :category};
    }
    let jobs = await models.Job.findAll(conditions);
    console.log(jobs);
    
  res.render('jobs/index', { jobs: jobs });
});

router.get('/create',async function(req, res, next) {

  let cats = await models.Category.findAll();
  let conditions ={
            model:models.User,
            attributes: { exclude: ['status'] }
    };
  let users = await models.User.findAll(conditions);
  res.render('jobs/create', { cats: cats , users : users });
});
// Store Jobs
router.post('/',async function(req, res, next) {
  let formData =  req.body;
  console.log(formData);
  let status = await models.Job.create(formData);
  res.redirect('/jobs');
});

// Store Edit
router.get('/edit/:id',async function(req, res, next) {
  let id = req.params.id || 0;
  let cats = await models.Category.findAll();
  let conditions ={
            model:models.User,
            attributes: { exclude: ['status'] }
    };
  let users = await models.User.findAll(conditions);
  let jobs = await models.Job.findOne({ where: {id: id} });
  res.render('jobs/edit',{ jobs: jobs, cats : cats, users : users });
});

// Store Update
router.post('/update/:id', async function(req, res, next) {

  let formData = req.body;
  let id = req.params.id || 0;
  
let status = await models.Job.update(formData, { where : { id : id }});
res.redirect('/jobs');

});

// Store Delete
router.post('/delete/:id', async function(req, res, next) {
  let id = req.params.id || 0;
  let status = await models.Job.destroy({ where : { id : id}});
  res.redirect('/jobs');
});

module.exports = router;
