var express = require('express');
var router = express.Router();
var models = require("../models");
/* GET home page. */
router.get('/', async function(req, res, next) {
  //  let role = 0;
    let conditions ={
      model:models.User,
      attributes: { exclude: ['status'] }
    };
   conditions.where = {role : 0};
  let users = await models.User.findAll(conditions);
  
  res.render('jobseekers/index', { userdata: users });
});
router.get('/create',async function(req, res, next) {

  let conditions ={
            model:models.User,
            attributes: { exclude: ['status'] }
    };
  let users = await models.User.findAll(conditions);
  res.render('jobseekers/create', { users : users });
});

// Store Jobs
router.post('/',async function(req, res, next) {
  let formData =  req.body;
  console.log(formData);
  let status = await models.User.create(formData);
  res.redirect('/jobseekers');
});
// Store Edit
router.get('/edit/:id',async function(req, res, next) {
  let id = req.params.id || 0;
  let conditions ={
            model:models.User,
            attributes: { exclude: ['status'] }
    };
  conditions.where = { id :id};
  let users = await models.User.findOne(conditions);
  res.render('jobseekers/edit',{ users : users });
});

router.post('/update/:id', async function(req, res, next) {

  let formData = req.body;
  let id = req.params.id || 0;
  
let status = await models.User.update(formData, { where : { id : id }});
res.redirect('/jobseekers');

});
// Store Delete
router.post('/delete/:id', async function(req, res, next) {
  let id = req.params.id || 0;
  let status = await models.User.destroy({ where : { id : id}});
  res.redirect('/jobseekers');
});


module.exports = router;
