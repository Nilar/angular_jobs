var express = require('express');
var router = express.Router();
var models = require("../models");
/* GET home page. */
router.get('/', async function(req, res, next) {
  //  let role = 0;
    let conditions ={
      include:[
        {
          model:models.Category
        },
      {
        model:models.User,
        attributes: { exclude: ['status'] }
      }]
    };
    let category = req.query.category;
   
  //conditions.where = { Resume.category_id :Category.id};
  let users = await models.Resume.findAll(conditions);
  
  res.render('resumes/index', { userdata: users });
});
router.get('/create',async function(req, res, next) {

  let conditions ={
            model:models.User,
            attributes: { exclude: ['status'] }
    };
    
  let users = await models.User.findAll(conditions);
  let category = await models.Category.findAll();
  res.render('resumes/create', { users : users , category : category });
});

// Store Jobs
router.post('/',async function(req, res, next) {
  
  let formData =  req.body;
  let status = await models.Resume.create(formData);
  res.redirect('/resumes');
});
// Store Edit
router.get('/edit/:id',async function(req, res, next) {
  let id = req.params.id || 0;
  let conditions ={
            model:models.User,
            attributes: { exclude: ['status'] }
    };
  conditions.where = { id :id};
  let resume = await models.Resume.findOne({ where: {id : id}});
  let user = await models.User.findAll();
  let category = await models.Category.findAll(); 
  res.render('resumes/edit',{ resume : resume  , category : category , user : user});
});

router.post('/update/:id', async function(req, res, next) {

  let formData = req.body;
  let id = req.params.id || 0;
  
let status = await models.Resume.update(formData, { where : { id : id }});
res.redirect('/resumes');

});
// Store Delete
router.post('/delete/:id', async function(req, res, next) {
  let id = req.params.id || 0;
  let status = await models.Resume.destroy({ where : { id : id}});
  res.redirect('/resumes');
});
module.exports = router;
