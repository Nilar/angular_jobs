-- phpMyAdmin SQL Dump
-- version 4.4.10
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: May 01, 2018 at 05:06 PM
-- Server version: 5.5.42
-- PHP Version: 5.6.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `ng_job`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(32) NOT NULL,
  `name` varchar(155) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'IT/Communications', '2018-03-15 13:02:21', '2018-05-01 09:23:42'),
(2, 'HR', '2018-03-15 13:02:42', '2018-05-01 09:23:50'),
(3, 'Digital Marketing', '2018-03-15 13:02:48', '2018-03-15 13:56:35'),
(4, 'Civil Engineers', '2018-03-15 13:03:00', '2018-05-01 09:41:13'),
(6, 'Admin', '2018-05-01 10:02:45', '2018-05-01 10:02:45');

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` int(32) NOT NULL,
  `title` varchar(155) NOT NULL,
  `description` text NOT NULL,
  `salary` varchar(55) NOT NULL,
  `category_id` int(32) unsigned NOT NULL,
  `agent_id` int(32) unsigned NOT NULL,
  `company` varchar(55) NOT NULL,
  `how_to_apply` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jobs`
--

INSERT INTO `jobs` (`id`, `title`, `description`, `salary`, `category_id`, `agent_id`, `company`, `how_to_apply`, `created_at`, `updated_at`) VALUES
(1, 'Frontend Web Developer', 'To Develop the websites', '100000', 3, 1, 'Web Solution', 'Email', '2018-03-24 07:46:25', '2018-05-01 11:01:18'),
(2, 'Site Engineer', 'To check the site', '500000', 4, 2, 'Aung Tagon', 'Email', '2018-03-24 08:10:41', '0000-00-00 00:00:00'),
(3, 'Manager', 'To Manage the human resourse', '500000', 2, 2, 'Myanmar', 'EMail', '2018-03-24 07:46:25', '0000-00-00 00:00:00'),
(5, 'Marketing Staff', 'To market our product', '300000', 3, 4, 'Thit San', 'By Person', '2018-03-24 08:04:12', '0000-00-00 00:00:00'),
(7, 'Software Engineer', 'PHP, .Net', '1000000', 1, 1, '.Net Company', 'Via Email', '2018-05-01 10:45:59', '2018-05-01 10:45:59');

-- --------------------------------------------------------

--
-- Table structure for table `resumes`
--

CREATE TABLE `resumes` (
  `id` int(32) NOT NULL,
  `title` varchar(155) NOT NULL,
  `description` text NOT NULL,
  `user_id` int(32) unsigned NOT NULL,
  `how_to_contact` varchar(155) NOT NULL,
  `category_id` int(32) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `resumes`
--

INSERT INTO `resumes` (`id`, `title`, `description`, `user_id`, `how_to_contact`, `category_id`, `created_at`, `updated_at`) VALUES
(1, 'Web Developer', '', 1, '', 1, '2018-03-16 12:29:18', '2018-03-16 12:29:18'),
(2, 'Hr Manager', '', 1, '', 2, '2018-03-16 12:29:38', '2018-03-16 12:29:38'),
(3, 'Junior Marketing Staff', '', 2, '', 3, '2018-03-16 12:30:47', '2018-03-16 12:30:47'),
(4, 'Site Engineer', '', 2, '', 4, '2018-03-16 12:31:23', '2018-03-16 12:31:23'),
(5, 'Senior Drafter', '', 3, '', 5, '2018-03-16 12:31:42', '2018-03-16 12:32:49');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(32) NOT NULL,
  `name` varchar(55) NOT NULL,
  `phone` varchar(55) NOT NULL,
  `email` varchar(55) DEFAULT NULL,
  `role` smallint(32) NOT NULL DEFAULT '1',
  `status` smallint(32) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `phone`, `email`, `role`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Nilar Naing Win', '90520057', 'nilarngwin@gmail.com', 0, 1, '2018-03-31 09:06:46', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `resumes`
--
ALTER TABLE `resumes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `resumes`
--
ALTER TABLE `resumes`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;